#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: glut 8.3.0 ruby lib
# stub: ext/glut/extconf.rb

Gem::Specification.new do |s|
  s.name = "glut".freeze
  s.version = "8.3.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Eric Hodel".freeze, "Lars Kanis".freeze, "Bla\u017E Hrastnik".freeze, "Alain Hoang".freeze, "Jan Dvorak".freeze, "Minh Thu Vo".freeze, "James Adam".freeze]
  s.date = "2017-06-02"
  s.description = "Glut bindings for OpenGL. To be used with the {opengl}[https://github.com/larskanis/opengl] gem.".freeze
  s.email = ["drbrain@segment7.net".freeze, "lars@greiz-reinsdorf.de".freeze, "speed.the.bboy@gmail.com".freeze, "".freeze, "".freeze, "".freeze, "".freeze]
  s.extensions = ["ext/glut/extconf.rb".freeze]
  s.extra_rdoc_files = ["History.rdoc".freeze, "Manifest.txt".freeze, "README.rdoc".freeze]
  s.files = [".autotest".freeze, ".gemtest".freeze, ".gitignore".freeze, ".travis.yml".freeze, "History.rdoc".freeze, "MIT-LICENSE".freeze, "Manifest.txt".freeze, "README.rdoc".freeze, "Rakefile".freeze, "ext/glut/common.h".freeze, "ext/glut/extconf.rb".freeze, "ext/glut/glut.c".freeze, "ext/glut/glut_callbacks.c".freeze, "ext/glut/glut_ext.c".freeze, "lib/glut.rb".freeze, "lib/glut/dummy.rb".freeze]
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--main".freeze, "README.rdoc".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.2".freeze)
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "Glut bindings for OpenGL".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<hoe>.freeze, ["~> 3.16"])
      s.add_development_dependency(%q<mini_portile2>.freeze, ["~> 2.1"])
      s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 1.0"])
      s.add_development_dependency(%q<rake-compiler-dock>.freeze, ["~> 0.6.0"])
      s.add_development_dependency(%q<rdoc>.freeze, ["~> 4.0"])
    else
      s.add_dependency(%q<hoe>.freeze, ["~> 3.16"])
      s.add_dependency(%q<mini_portile2>.freeze, ["~> 2.1"])
      s.add_dependency(%q<rake-compiler>.freeze, ["~> 1.0"])
      s.add_dependency(%q<rake-compiler-dock>.freeze, ["~> 0.6.0"])
      s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
    end
  else
    s.add_dependency(%q<hoe>.freeze, ["~> 3.16"])
    s.add_dependency(%q<mini_portile2>.freeze, ["~> 2.1"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 1.0"])
    s.add_dependency(%q<rake-compiler-dock>.freeze, ["~> 0.6.0"])
    s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
  end
end
